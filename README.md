# Revert Review API
API for the revert review tool on Toolforge. Check out the [docs](https://revert-review-api.toolforge.org/v1) to learn more.

## Development

### Prerequisites
* Install the latest version of [Poetry](https://python-poetry.org/docs/#installation)

### Setup
After installing the prerequisites, run:
```bash
git clone https://gitlab.wikimedia.org/repos/research/revert-review-api.git
cd revert-review-api
poetry install --no-root
```
Add your `config.yaml` and then run:
```bash
flask --app revert_review_api/app init-db
flask --app revert_review_api/app run
```

### Testing
To run all tests:
```bash
poetry run pytest -vv
```

### Hooks
To set up the pre-commit hooks, run:
```bash
poetry run pre-commit install
```
This will lint and typecheck the code on every commit. Hooks and tests are also run in the CI.

## Toolforge Deployment
To set up `Revert Review API` on Toolforge:
```bash
mkdir -p $HOME/www/python && cd $_
git clone https://gitlab.wikimedia.org/repos/research/revert-review-api.git src
cd src
bash deploy.sh -d
```
This will do a few things:
* Create a user database on Toolforge if it does not exist.
* Create a config.yaml and add the database uri to it.
* Create all tables.
* Add a virtual enviroment and install all dependencies.
* Start the webservice.

To update:
```bash
git pull --ff-only
bash deploy.sh
```
This will install any new dependencies in the virtual environment and restart the webservice. Notice the lack of option `-d` here. This is because we don't want to set up and re-initialize the database every time we update the API.
