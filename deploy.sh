#!/bin/bash
#
# Set up and deploy Revert Review API on Toolforge

set -o errexit
set -o pipefail
set -o nounset

ROOT="${HOME}/www/python"
MY_CNF="${HOME}/replica.my.cnf"
SRC="${ROOT}/src"
HOSTNAME='tools.db.svc.wikimedia.cloud'

#######################################
# Prints a message in the given color.
# Arguments:
#   $1: message
#   $2: color
# Outputs:
#   Writes message to stdout
#######################################
color_print() {
    # Set the color to the given color
    echo -n -e "$2"
    echo "$1"
    # Reset the color to no color
    echo -n -e "\e[0m"
}

#######################################
# Prints an error message in red.
# Arguments:
#   $1: message
# Outputs:
#   Writes message to stdout
#######################################
err() {
    color_print "ERROR: $1" "\e[31m"
}

#######################################
# Prints an info message in green.
# Arguments:
#   $1: message
# Outputs:
#   Writes message to stdout
#######################################
info() {
    color_print "=> $1" "\e[32m"
}

#######################################
# Creates (optionally) and activates a venv with API deps.
# Globals:
#   ROOT
#   SRC
#######################################
activate_venv() {
    local venv_path="${ROOT}/venv"

    if [[ ! -d "${venv_path}" ]]; then
        python3 -m venv "${venv_path}"
    fi
    # Workaround for https://github.com/pypa/virtualenv/issues/1342
    sed -i 's/$1/${1:-}/' "${venv_path}/bin/activate"
    source "${venv_path}/bin/activate"

    python3 -m pip install -U pip setuptools
    python3 -m pip install -e "${SRC}"
}

#######################################
# Gets the specified field from ~/.replica.my.cnf.
# Globals:
#   MY_CNF
# Arguments:
#   $1: field, such as 'user'
# Outputs:
#   Writes field value to stdout
#######################################
extract_from_my_cnf() {
    if [[ ! -f "${MY_CNF}" ]]; then
        err "Couldn't find ${MY_CNF}"
        exit 1
    fi

    awk '/^'"$1"'/ {print $3}' "${MY_CNF}"
}

#######################################
# Creates a mysql db if it does not exist.
# Globals:
#   MY_CNF
#   HOSTNAME
# Arguments:
#   $1: db name, without Toolforge specific affixes
# Outputs:
#   Writes Toolforge db name to stdout.
#######################################
create_db () {
    if [[ ! -f "${MY_CNF}" ]]; then
        err "Couldn't find ${MY_CNF}"
        exit 1
    fi

    local user
    user=$( extract_from_my_cnf 'user' )

    local db_name="${user}__$1_p"

    mysql --defaults-file="${MY_CNF}" \
    -h "${HOSTNAME}" \
    -e "CREATE DATABASE IF NOT EXISTS ${db_name} CHARACTER SET utf8;"

    echo "${db_name}"
}

#######################################
# Generates a mysql DSN.
# Arguments:
#   $1: Toolforge db name
# Globals:
#   HOSTNAME
# Outputs:
#   Writes 'DATABASE_URI: {mysql DSN}' to stdout
#######################################
generate_db_uri() {
    local password
    password=$( extract_from_my_cnf 'password' )

    local user
    user=$( extract_from_my_cnf 'user' )

    local port=3306

    echo "DATABASE_URI: mysql+pymysql://${user}:${password}@${HOSTNAME}:${port}/$1"
}

#######################################
# Creates a symlink to wsgi.py named app.py which is expected by Toolforge.
# Globals:
#   SRC
#######################################
create_wsgi_entrypoint() {
    local entrypoint="${SRC}/app.py"

    if [[ ! -L "${entrypoint}" ]]; then
        ln -s "${SRC}/wsgi.py" "${entrypoint}"
    fi
}

#######################################
# Main
#######################################
main() {
    local db_name='revert_review'
    local setup_db='false'

    while getopts 'd' flag; do
        case "${flag}" in
            d) setup_db='true' ;;
            *) err "Unexpected option ${flag}"; exit 1 ;;
        esac
    done

    if [[ "${setup_db}" == 'true' ]]; then
        prompt='You ran the script with -d option. '
        prompt+='Are you sure you want to drop all existing tables '
        prompt+='and reinitialize the database if it exists (Y/y)?'

        read -p "${prompt}" -n 1 -r
        echo

        if [[ $REPLY =~ ^[Yy]$ ]]; then
            info 'Creating mysql database if it does not exist'
            local toolforge_db_name
            toolforge_db_name=$( create_db "${db_name}" )
            info "Database ${toolforge_db_name} has been created"

            info "Writing Database URI to config.yaml"
            generate_db_uri "${toolforge_db_name}" >> "${SRC}/config.yaml"
        else
            info 'Aborting database initialization'
            info 'Following steps assume that the db and tables exist'
            setup_db='false'
        fi
    fi

    info 'Installing dependencies'
    activate_venv

    if [[ "${setup_db}" == 'true' ]]; then
        cd "${SRC}"
        info 'Initializing the database'
        flask --app revert_review_api/app init-db
    fi

    deactivate

    info 'Creating wsgi entrypoint: app.py'
    create_wsgi_entrypoint

    info 'Restarting webservice'
    if ! webservice restart; then
        info 'Webservice is not running. Starting webservice'
        webservice --backend=kubernetes python3.7 start
    fi
}

main "$@"
