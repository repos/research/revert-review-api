from http import HTTPStatus
from typing import Dict, Generator

import pytest

from flask import Flask
from flask.testing import FlaskClient
from vcr import VCR

from revert_review_api import models, schemas
from revert_review_api.app import Config, create_app
from revert_review_api.database import db


mwvcr = VCR(
    cassette_library_dir="tests/fixtures/cassettes",
    path_transformer=VCR.ensure_suffix(".yaml"),
    decode_compressed_response=True,
    record_mode="once",
)


@pytest.fixture(scope="class")
def app() -> Generator[Flask, None, None]:
    config = Config(DATABASE_URI="sqlite:///:memory:")
    app = create_app(config)

    with app.app_context():
        # set up
        db.create_all()

        yield app

        # tear down
        db.drop_all()


@pytest.fixture(scope="class")
def client(app: Flask) -> FlaskClient:
    return app.test_client()


class TestRecentchanges:
    base_url = "/v1/recentchanges"

    @mwvcr.use_cassette()
    def test_get_changes(self, client: FlaskClient) -> None:
        response = client.get(f"{self.base_url}/enwiki")
        assert response.status_code == HTTPStatus.OK

        data = response.json
        assert data is not None

        result = schemas.RecentChanges(**data)
        assert len(result.revisions) == 10  # noqa: PLR2004

    def test_get_changes_no_wiki(self, client: FlaskClient) -> None:
        response = client.get(f"{self.base_url}")
        assert response.status_code == HTTPStatus.NOT_FOUND

    def test_get_changes_invalid_wiki(self, client: FlaskClient) -> None:
        wiki = "1.2.3.4"
        response = client.get(f"{self.base_url}/{wiki}")
        assert response.status_code == HTTPStatus.BAD_REQUEST

        wiki = "https://example.com"
        response = client.get(f"{self.base_url}/{wiki}")
        assert response.status_code == HTTPStatus.NOT_FOUND

    @mwvcr.use_cassette()
    def test_get_changes_over_limit(self, client: FlaskClient) -> None:
        response = client.get(f"{self.base_url}/enwiki?limit=100")
        assert response.status_code == HTTPStatus.BAD_REQUEST


class TestRevisions:
    base_url = "/v1/revisions"

    @mwvcr.use_cassette()
    def test_get_revision(self, client: FlaskClient) -> None:
        response = client.get(f"{self.base_url}/enwiki/12345|12342")
        assert response.status_code == HTTPStatus.OK

        data = response.json
        assert data is not None

        result = schemas.RetrievedRevisions(**data)
        assert len(result.revisions) == 2  # noqa: PLR2004
        assert len(result.missing) == 0

    def test_get_revision_invalid_wiki(self, client: FlaskClient) -> None:
        response = client.get(f"{self.base_url}/en_wiki/12345")
        assert response.status_code == HTTPStatus.BAD_REQUEST

    def test_get_revision_invalid_id(self, client: FlaskClient) -> None:
        response = client.get(f"{self.base_url}/enwiki/123a")
        assert response.status_code == HTTPStatus.BAD_REQUEST

    @mwvcr.use_cassette()
    def test_get_revision_missing_id(self, client: FlaskClient) -> None:
        response = client.get(f"{self.base_url}/enwiki/48511104318")
        assert response.status_code == HTTPStatus.OK

        data = response.json
        assert data is not None

        result = schemas.RetrievedRevisions(**data)
        assert len(result.revisions) == 0
        assert len(result.missing) == 1


class TestReview:
    base_url = "/v1/review"

    @mwvcr.use_cassette()
    def test_create_review(self, client: FlaskClient) -> None:
        id = 12345
        wiki = "enwiki"
        body = {"label": "correct"}
        response = client.post(f"{self.base_url}/{wiki}/{id}", json=body)
        assert response.status_code == HTTPStatus.OK

        data = response.json
        assert data is not None

        review = schemas.Review(**data)
        assert review.id == id
        assert review.wiki == wiki
        assert review.correct == 1
        assert review.unsure == 0
        assert review.incorrect == 0

    def test_create_review_invalid_id(self, client: FlaskClient) -> None:
        body = {"label": "correct"}
        response = client.post(f"{self.base_url}/enwiki/123a", json=body)
        assert response.status_code == HTTPStatus.NOT_FOUND

    def test_create_review_invalid_wiki(self, client: FlaskClient) -> None:
        body = {"label": "correct"}
        response = client.post(f"{self.base_url}/en_wiki/12345", json=body)
        assert response.status_code == HTTPStatus.BAD_REQUEST

    @pytest.mark.parametrize(
        "body", ({"babel": "correct"}, {"label": "correc"}, {"label": "unsure "})
    )
    def test_create_review_invalid_label(
        self, client: FlaskClient, body: Dict[str, str]
    ) -> None:
        response = client.post(f"{self.base_url}/enwiki/12345", json=body)
        assert response.status_code == HTTPStatus.BAD_REQUEST

    def test_retrieve_review(self, client: FlaskClient) -> None:
        id = 12342
        wiki = "enwiki"

        review = models.Review(
            id=id,
            wiki=wiki,
            model_version="0.5.0",
            prediction="False",
            probability=0.25,
            unsure=1,
        )
        db.session.add(review)

        response = client.get(f"{self.base_url}/{wiki}/{id}")
        assert response.status_code == HTTPStatus.OK

        data = response.json
        assert data is not None

        assert schemas.Review(**data) == schemas.Review.from_orm(review)

    def test_retrieve_unexisting_review(self, client: FlaskClient) -> None:
        id = 12334
        wiki = "enwiki"

        review = models.Review.query.filter_by(id=id, wiki=wiki).first()
        assert review is None

        response = client.get(f"{self.base_url}/enwiki/12334")
        assert response.status_code == HTTPStatus.NOT_FOUND
