from vcr import VCR

from revert_review_api.mediawiki import (
    get_recent_changes,
    get_revision_score,
    get_revisions,
)
from revert_review_api.schemas import (
    ModelResult,
    ModelScore,
    RecentChanges,
    RetrievedRevisions,
    Revision,
)


mwvcr = VCR(
    cassette_library_dir="tests/fixtures/cassettes",
    path_transformer=VCR.ensure_suffix(".yaml"),
    decode_compressed_response=True,
    record_mode="once",
)


@mwvcr.use_cassette()
def test_get_revision_score() -> None:
    wiki = "arwiki"
    rev_ids = [3485434, 48511104318]
    expected = ModelResult(
        version="0.5.0",
        scores={
            # 48511104318 not present since it's invalid
            3485434: ModelScore(prediction="False", probability=0.09680325099238377)
        },
    )

    assert get_revision_score(wiki, rev_ids) == expected


@mwvcr.use_cassette()
def test_get_recent_changes() -> None:
    wiki = "enwiki"
    limit = 1
    expected = RecentChanges(
        revisions=[
            Revision(
                comment="Country of birth",
                id=1135506400,
                parent_id=1135487887,
                score=ModelScore(prediction="False", probability=0.3014132994715135),
                title="Devin Lloyd",
                timestamp="2023-01-25T01:54:47Z",
                user="Bringingthewood",
                wiki="enwiki",
            )
        ]
    )

    result = get_recent_changes(wiki, limit)

    assert result is not None
    assert result.revisions == expected.revisions


@mwvcr.use_cassette()
def test_get_revisions() -> None:
    wiki = "enwiki"
    rev_ids = [12345, 1135506400, 45666666666666111, 67890]
    expected = RetrievedRevisions(
        revisions=[
            Revision(
                comment="*",
                id=12345,
                parent_id=12342,
                score=ModelScore(prediction="False", probability=0.255723979808171),
                title="Congruence (geometry)",
                timestamp="2002-02-16T19:00:49Z",
                user="210.55.230.17",
                wiki="enwiki",
            ),
            Revision(
                comment="Country of birth",
                id=1135506400,
                parent_id=1135487887,
                score=ModelScore(prediction="False", probability=0.3014132994715135),
                title="Devin Lloyd",
                timestamp="2023-01-25T01:54:47Z",
                user="Bringingthewood",
                wiki="enwiki",
            ),
            Revision(
                comment="*",
                id=67890,
                parent_id=67877,
                score=ModelScore(prediction="False", probability=0.12584571561379182),
                title="SECAM",
                timestamp="2002-04-27T03:35:17Z",
                user="Roadrunner",
                wiki="enwiki",
            ),
        ],
        missing=[45666666666666111],
    )

    result = get_revisions(wiki, rev_ids)

    assert result is not None
    assert result.missing == expected.missing
    assert set(result.revisions) == set(expected.revisions)
