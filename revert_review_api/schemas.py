import datetime
import enum

from typing import Dict, List, Optional

from pydantic import BaseModel, Field


class CustomBaseModel(BaseModel):
    class Config:
        frozen = True


class ModelScore(CustomBaseModel):
    prediction: str
    probability: float


class ModelResult(CustomBaseModel):
    version: str
    scores: Dict[int, ModelScore]


class Revision(CustomBaseModel):
    comment: str
    id: int
    parent_id: int
    # score can be None for certain revisions
    # that don't have all information required
    # depending on the model used
    score: Optional[ModelScore]
    timestamp: datetime.datetime
    title: str
    user: str
    wiki: str


class RetrievedRevisions(CustomBaseModel):
    revisions: List[Revision]
    missing: List[int]


class RecentChanges(CustomBaseModel):
    revisions: List[Revision]


class RecentChangesGet(CustomBaseModel):
    limit: int = Field(10, gt=0, le=50)


class Review(CustomBaseModel):
    id: int
    wiki: str
    prediction: str
    probability: float
    correct: int
    unsure: int
    incorrect: int
    time_created: datetime.datetime
    time_updated: datetime.datetime

    class Config:
        orm_mode = True


class Label(str, enum.Enum):
    CORRECT = "correct"
    UNSURE = "unsure"
    INCORRECT = "incorrect"


class ReviewCreate(CustomBaseModel):
    label: Label
