import re

from typing import List

from werkzeug.exceptions import BadRequest
from werkzeug.routing import BaseConverter


class IdsConverter(BaseConverter):
    description = "Pipe delimited revision ids"

    def to_python(self, value: str) -> List[int]:
        id_tokens = value.split("|")
        for id in id_tokens:
            if re.fullmatch(r"[0-9]+", id) is None:
                raise BadRequest(f"{id} is invalid")

        return [int(id) for id in id_tokens]


class WikiConverter(BaseConverter):
    description = "wiki should end with 'wiki'"

    def to_python(self, value: str) -> str:
        if re.fullmatch(r"[a-zA-Z-]+wiki", value) is None:
            raise BadRequest(f"Invalid wiki '{value}'")

        return value
