from pathlib import Path
from typing import Optional

import flask
import yaml

from pydantic.dataclasses import dataclass
from werkzeug.exceptions import HTTPException

from revert_review_api.database import db, init_db_command
from revert_review_api.utils.converters import IdsConverter, WikiConverter
from revert_review_api.utils.error_handlers import handle_http_exception
from revert_review_api.utils.json import CustomJSONProvider


@dataclass(frozen=True)
class Config:
    DATABASE_URI: str

    @classmethod
    def from_yaml(
        cls,
        root_path: Path = Path(__file__).resolve().parent.parent,
        filename: str = "config.yaml",
    ) -> "Config":
        config = flask.Config(str(root_path))
        config.from_file(filename, load=yaml.safe_load)

        return Config(**config)


def create_app(config: Optional[Config] = None) -> flask.Flask:
    """Create and configure an instance of the Flask application."""

    app = flask.Flask(__name__)

    if config is None:
        config = Config.from_yaml()

    app.config["SQLALCHEMY_DATABASE_URI"] = config.DATABASE_URI
    db.init_app(app)
    app.cli.add_command(init_db_command)

    app.json = CustomJSONProvider(app)
    app.url_map.converters.update({"ids": IdsConverter, "wiki": WikiConverter})
    app.register_error_handler(HTTPException, handle_http_exception)

    from revert_review_api.api import api

    app.register_blueprint(api)

    return app
