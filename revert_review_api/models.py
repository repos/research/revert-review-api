import datetime

from sqlalchemy import Column, DateTime, Float, Index, Integer, String

from revert_review_api.database import db


utc_now = datetime.datetime.utcnow


class Review(db.Model):  # type: ignore
    id = Column(Integer, primary_key=True)
    # mysql requires that length be specified for strings
    wiki = Column(String(length=50), primary_key=True)
    model_version = Column(String(length=50), primary_key=True)
    prediction = Column(String(length=50))
    probability = Column(Float)
    correct = Column(Integer, autoincrement=False, default=0)
    unsure = Column(Integer, autoincrement=False, default=0)
    incorrect = Column(Integer, autoincrement=False, default=0)
    time_created = Column(DateTime(timezone=True), default=utc_now)
    time_updated = Column(DateTime(timezone=True), default=utc_now, onupdate=utc_now)

    __table_args__ = (Index("idx_review_id_wiki", id, wiki),)
