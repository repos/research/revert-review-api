import click

from flask_sqlalchemy import SQLAlchemy


db: SQLAlchemy = SQLAlchemy()


def init_db() -> None:
    db.drop_all()
    db.create_all()


@click.command("init-db")
def init_db_command() -> None:
    """Clear existing data and create new tables."""
    init_db()
    click.echo("Initialized the database.")
