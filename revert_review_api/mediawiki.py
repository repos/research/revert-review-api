import logging

from typing import Dict, Optional, Sequence, Union

import requests

from revert_review_api.schemas import (
    ModelResult,
    ModelScore,
    RecentChanges,
    RetrievedRevisions,
    Revision,
)


def get_revision_score(wiki: str, rev_ids: Sequence[int]) -> Optional[ModelResult]:
    """Returns a `dict` that maps all valid revision ids in `rev_ids`
    to a `damaging` score if the request to the model's API succeeds,
    otherwise returns `None`.
    """
    try:
        url = f"https://ores.wikimedia.org/v3/scores/{wiki}"
        rev_ids_str = "|".join(str(id) for id in rev_ids)
        params = {"models": "damaging", "revids": rev_ids_str}
        response = requests.get(url, params=params).json()
        version = response[wiki]["models"]["damaging"]["version"]
        scores = response[wiki]["scores"]

        score_map = {}
        for rev_id, result in scores.items():
            if "score" in result["damaging"]:
                score = result["damaging"]["score"]
                score_map[rev_id] = ModelScore(
                    prediction=score["prediction"],
                    probability=score["probability"]["true"],
                )

        return ModelResult(version=version, scores=score_map)
    except Exception:
        logging.exception("Could not fetch revision scores")
        return None


def get_recent_changes(wiki: str, limit: int = 10) -> Optional[RecentChanges]:
    """Returns a `RecentChanges` instance if the request to MW Api
    succeeds  and if it's able to fetch revision scores for the retrieved
    revisions, otherwise returns `None`.
    """
    try:
        lang = wiki.replace("wiki", "")
        url = f"https://{lang}.wikipedia.org/w/api.php"
        params: Dict[str, Union[int, str]] = {
            "action": "query",
            "list": "recentchanges",
            "format": "json",
            "formatversion": 2,
            "prop": "info",
            "rcnamespace": 0,
            "rclimit": limit,
            "rcshow": "!bot",
            "rctype": "edit",
            "rctoponly": "true",
            "rcprop": "comment|user|timestamp|ids|title",
        }
        response = requests.get(url, params=params).json()
        recentchanges = response["query"]["recentchanges"]

        # fetch scores for all revisions returned in recent changes
        recentchanges_ids = [rc["revid"] for rc in recentchanges]
        revision_result = get_revision_score(wiki, recentchanges_ids)
        if revision_result is None:
            return None

        revision_scores = revision_result.scores

        revisions = [
            Revision(
                comment=rc["comment"],
                id=rc["revid"],
                parent_id=rc["old_revid"],
                score=revision_scores.get(rc["revid"]),
                timestamp=rc["timestamp"],
                title=rc["title"],
                user=rc["user"],
                wiki=wiki,
            )
            for rc in recentchanges
        ]
        return RecentChanges(revisions=revisions)
    except Exception:
        logging.exception("Could not fetch recent changes")
        return None


def get_revisions(wiki: str, rev_ids: Sequence[int]) -> Optional[RetrievedRevisions]:
    """Returns a `RetrievedRevisions` instance if the request to MW Api
    succeeds  and if it's able to fetch revision scores for the retrieved
    revisions, otherwise returns `None`.
    """
    try:
        lang = wiki.replace("wiki", "")
        url = f"https://{lang}.wikipedia.org/w/api.php"
        rev_ids_str = "|".join(str(id) for id in rev_ids)
        params: Dict[str, Union[int, str]] = {
            "action": "query",
            "format": "json",
            "formatversion": 2,
            "prop": "revisions",
            "revids": rev_ids_str,
            "rvslots": "main",
            "rvprop": "comment|user|timestamp|ids",
        }
        response = requests.get(url, params=params).json()

        # fetch scores for all revisions in rev_ids
        revision_result = get_revision_score(wiki, rev_ids)
        if revision_result is None:
            return None

        revision_scores = revision_result.scores

        revisions = []
        if "pages" in response["query"]:
            for page in response["query"]["pages"]:
                for rev in page["revisions"]:
                    revision = Revision(
                        comment=rev["comment"],
                        id=rev["revid"],
                        parent_id=rev["parentid"],
                        score=revision_scores.get(rev["revid"]),
                        timestamp=rev["timestamp"],
                        title=page["title"],
                        user=rev["user"],
                        wiki=wiki,
                    )
                    revisions.append(revision)

        missing_revisions = list(response["query"].get("badrevids", {}).keys())

        return RetrievedRevisions(revisions=revisions, missing=missing_revisions)
    except Exception:
        logging.exception("Could not fetch revisions")
        return None
