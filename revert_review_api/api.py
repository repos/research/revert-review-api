from http import HTTPStatus
from typing import Sequence

import spectree
import sqlalchemy.exc

from flask import Blueprint, Response, redirect
from werkzeug.exceptions import BadRequest, InternalServerError, NotFound

from revert_review_api import models, schemas
from revert_review_api.database import db
from revert_review_api.mediawiki import (
    get_recent_changes,
    get_revision_score,
    get_revisions,
)
from revert_review_api.utils.converters import IdsConverter, WikiConverter


api = Blueprint("api", __name__, url_prefix="/v1")

spec = spectree.SpecTree(
    "flask",
    path="docs",
    annotations=True,
    mode="strict",
    title="Revert Review API",
    validation_error_status=HTTPStatus.BAD_REQUEST,
)


@api.route("/")
def docs() -> Response:
    return redirect("/v1/docs/swagger", code=HTTPStatus.SEE_OTHER)


@api.route("/recentchanges/<wiki:wiki>")
@spec.validate(
    resp=spectree.Response(HTTP_200=schemas.RecentChanges),
    path_parameter_descriptions={"wiki": WikiConverter.description},
)
def recentchanges(wiki: str, query: schemas.RecentChangesGet) -> schemas.RecentChanges:
    """Get recent changes from the MediaWiki API along
    with their revert-risk score.
    """
    recent_changes = get_recent_changes(wiki, query.limit)
    if recent_changes is None:
        raise InternalServerError
    return recent_changes


@api.route("/revisions/<wiki:wiki>/<ids:ids>")
@spec.validate(
    resp=spectree.Response(HTTP_200=schemas.RetrievedRevisions),
    path_parameter_descriptions={
        "wiki": WikiConverter.description,
        "ids": IdsConverter.description,
    },
)
def revisions(wiki: str, ids: Sequence[int]) -> schemas.RetrievedRevisions:
    """Get revisions from the MediaWiki API along with
    their revert-risk score.
    """
    revisions = get_revisions(wiki, ids)
    if revisions is None:
        raise InternalServerError
    return revisions


@api.route("/review/<wiki:wiki>/<int:id>", methods=["POST"])
@spec.validate(
    resp=spectree.Response(HTTP_200=schemas.Review),
    path_parameter_descriptions={"wiki": WikiConverter.description},
)
def create_review(wiki: str, id: int, json: schemas.ReviewCreate) -> schemas.Review:
    """Add a review for the revert risk score generated for
    the revision identified by `wiki` and `id`.
    """
    revision_result = get_revision_score(wiki, (id,))
    if revision_result is None:
        raise InternalServerError

    model_version = revision_result.version
    score = revision_result.scores.get(id)
    if score is None:
        raise BadRequest(
            f"Review invalid. Model currently has no score for {wiki}: {id}"
        )

    label = json.label

    # Checking that the review already exists before adding one
    # could lead to a race condition where multiple processes
    # could try to insert at the same time and when some of them
    # fail with an IntegrityError, we'll have to do another round
    # trip to the DB to get the review and then update it.
    # The following takes a different approach by always trying
    # to insert first and only retrieving the review if that fails
    # which reduces the total number of round trips to the DB.
    # It does however favor insert heavy use cases over update heavy ones.
    try:
        review = models.Review(
            id=id,
            wiki=wiki,
            model_version=model_version,
            prediction=score.prediction,
            probability=score.probability,
        )
        setattr(review, label, 1)
        db.session.add(review)
        db.session.flush()

    except sqlalchemy.exc.IntegrityError:
        db.session.rollback()
        review = db.session.get(models.Review, (id, wiki, model_version))  # type: ignore[assignment]  # noqa: E501
        setattr(review, label, getattr(models.Review, label) + 1)

    db.session.commit()

    return schemas.Review.from_orm(review)


@api.route("/review/<wiki:wiki>/<int:id>")
@spec.validate(
    resp=spectree.Response(HTTP_200=schemas.Review),
    path_parameter_descriptions={"wiki": WikiConverter.description},
)
def review(wiki: str, id: int) -> schemas.Review:
    """Get reviews for the revert risk score associated with the
    revision identified by `id` and `wiki`.
    """
    review = (
        models.Review.query.filter_by(id=id, wiki=wiki)
        .order_by(models.Review.time_created.desc())
        .first()
    )

    if review is None:
        raise NotFound(f"Could not find any reviews for {wiki}: {id}")

    return schemas.Review.from_orm(review)


spec.register(api)
